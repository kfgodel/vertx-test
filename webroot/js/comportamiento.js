var auction_id = 1;

function init() {
  eventBus = new EventBus('http://localhost:8080/eventbus');
  eventBus.onopen = function () {
    registrarHandlers();
    consultarPrecioActual();
  };
}

function registrarHandlers() {
  registraHandlerParaActualizacionDePrecio();
  registrarHandlerParaNuevaOferta();
}

function registraHandlerParaActualizacionDePrecio() {
  eventBus.registerHandler('auction.' + auction_id + '.price', function (error, message) {
    if (error) {
      console.log("Error");
      console.log(error);
    }
    document.getElementById('current_price').innerHTML = 'EUR ' + JSON.parse(message.body);
  });
}

function registrarHandlerParaNuevaOferta() {
  eventBus.registerHandler('auction.' + auction_id + '.bid', function (error, message) {
    if (error) {
      console.log("Error");
      console.log(error);
    }
    document.getElementById('feed').value += 'New offer: EUR ' + JSON.parse(message.body).price + '\n';
  });
}

function consultarPrecioActual() {
  eventBus.send("auction.price", JSON.stringify(auction_id), function (error, response) {
    let body = response.body;
    console.log("Respuesta en : " + response.address + ": " + body);
    document.getElementById('current_price').innerHTML = 'EUR ' + JSON.parse(body);
  });
}

function bid() {
  var biddingInput = document.getElementById('my_bid_value');
  var newPrice = parseFloat(Math.round(biddingInput.value.replace(',', '.') * 100) / 100).toFixed(2);

  eventBus.publish("auction.bid", JSON.stringify({id: auction_id, price: newPrice}));

  biddingInput.value = '';
}
