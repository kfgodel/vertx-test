package ar.com.kfgodel.vertx;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Date: 27/01/18 - 23:00
 */
public class MainBid {
  public static Logger LOG = LoggerFactory.getLogger(MainBid.class);

  public static void main(String[] args) {
    EjemploRemateConWebsockets.create().iniciar();

//    pruebas();
  }

  private static void pruebas() {
    Vertx vertx = Vertx.vertx();
    imprimirThreadActual();


    Future<String> startFuture = Future.future();

    Future<String> fut1 = Future.future();
    fut1.complete("paso 1");

    fut1.compose(v -> {
      // When the file is created (fut1), execute this:
      Future<String> fut2 = Future.future();
      fut2.complete(v + ", paso 2");
      return fut2;
    }).compose(v -> {
        // When the file is written (fut2), execute this:
        startFuture.complete(v + ", paso 3");
      },
      // mark startFuture it as failed if any step fails.
      startFuture);

    LOG.info("Resultado: {}", startFuture.result());
  }

  private static void imprimirThreadActual() {
    LOG.info("Imprimiendo thread");
  }

}
