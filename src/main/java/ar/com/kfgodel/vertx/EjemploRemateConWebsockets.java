package ar.com.kfgodel.vertx;

import ar.com.kfgodel.vertx.ejemplo.AuctionMessageHandler;
import ar.com.kfgodel.vertx.ejemplo.LogicaDelRemate;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.ext.bridge.BridgeEventType;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.ErrorHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Date: 28/01/18 - 18:36
 */
public class EjemploRemateConWebsockets {
  public static Logger LOG = LoggerFactory.getLogger(EjemploRemateConWebsockets.class);

  private Vertx vertx;

  public static EjemploRemateConWebsockets create() {
    EjemploRemateConWebsockets ejemplo = new EjemploRemateConWebsockets();
    ejemplo.vertx = Vertx.vertx();
    return ejemplo;
  }

  public void iniciar() {
    ClusterManager mgr = new HazelcastClusterManager();
    VertxOptions options = new VertxOptions().setClusterManager(mgr);
    Vertx.clusteredVertx(options, res -> {
      if (res.succeeded()) {
        this.vertx = res.result();
        registrarHandlerDeMensajesEnElBus();
        iniciarServerHttpConWebSockets();
      } else {
        LOG.info("Fallo la obtencion de vertex", res.cause());
      }
    });
  }

  private void registrarHandlerDeMensajesEnElBus() {
    LogicaDelRemate logicaDelRemate = LogicaDelRemate.create(vertx.eventBus());

    AuctionMessageHandler messageHandler = AuctionMessageHandler.create(logicaDelRemate);
    messageHandler.registrarEn(vertx.eventBus());
  }

  private Router crearRouterParaWebsockets() {
    Router router = Router.router(vertx);
    router.route("/eventbus/*").handler(eventBusHandler());
    router.route().failureHandler(errorHandler());
    router.route().handler(staticHandler());
    return router;
  }

  private SockJSHandler eventBusHandler() {
    BridgeOptions options = new BridgeOptions()
      .addOutboundPermitted(new PermittedOptions().setAddressRegex("auction.+"))
      .addInboundPermitted(new PermittedOptions().setAddressRegex("auction.+"));
    return SockJSHandler.create(vertx)
      .bridge(options, event -> {
        if (event.type() == BridgeEventType.SOCKET_CREATED) {
          LOG.info("A socket was created");
        }
        event.complete(true);
      });
  }

  private ErrorHandler errorHandler() {
    return ErrorHandler.create(true);
  }

  private StaticHandler staticHandler() {
    return StaticHandler.create()
      .setCachingEnabled(false);
  }

  private void iniciarServerHttpConWebSockets() {
    Router router = crearRouterParaWebsockets();
    vertx.createHttpServer()
      .requestHandler(router::accept)
      .listen(8080);
  }

}
